<?php

namespace App\Controller;

use App\Entity\Quotes;
use App\Form\QuotesType;
use App\Repository\QuotesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class QuotesController extends AbstractController
{
    /**
     * @Route("/", name="app_quotes_index", methods={"GET"})
     */
    public function index(QuotesRepository $quotesRepository): Response
    {
        return $this->render('quotes/index.html.twig', [
            'quotes' => $quotesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_quotes_new", methods={"GET", "POST"})
     */
    public function new(Request $request, QuotesRepository $quotesRepository): Response
    {
        $quote = new Quotes();
        $form = $this->createForm(QuotesType::class, $quote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $quotesRepository->add($quote, true);

            return $this->redirectToRoute('app_quotes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('quotes/new.html.twig', [
            'quote' => $quote,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_quotes_show", methods={"GET"})
     */
    public function show(Quotes $quote): Response
    {
        return $this->render('quotes/show.html.twig', [
            'quote' => $quote,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_quotes_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Quotes $quote, QuotesRepository $quotesRepository): Response
    {
        $form = $this->createForm(QuotesType::class, $quote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $quotesRepository->add($quote, true);

            return $this->redirectToRoute('app_quotes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('quotes/edit.html.twig', [
            'quote' => $quote,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_quotes_delete", methods={"POST"})
     */
    public function delete(Request $request, Quotes $quote, QuotesRepository $quotesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$quote->getId(), $request->request->get('_token'))) {
            $quotesRepository->remove($quote, true);
        }

        return $this->redirectToRoute('app_quotes_index', [], Response::HTTP_SEE_OTHER);
    }
}
